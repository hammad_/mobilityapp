package com.hammadmukhtar.mobilityapp.interfaces;

import com.hammadmukhtar.mobilityapp.data.models.CarLocation;

import java.util.List;

/**
 * Created by Hammad Mukhtar on 02-Dec-17.
 */

public interface ILocation {
    void updateList(List<CarLocation> places);
}
