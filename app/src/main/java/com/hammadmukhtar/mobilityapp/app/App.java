package com.hammadmukhtar.mobilityapp.app;

import android.app.Application;
import android.content.Context;

import com.hammadmukhtar.mobilityapp.data.db.DatabaseHelper;
import com.j256.ormlite.android.apptools.OpenHelperManager;

/**
 * Created by Hammad Mukhtar on 02-Dec-17.
 */

public class App extends Application {

    private static Context context;
    public static DatabaseHelper databaseHelper;

    @Override
    public void onCreate() {
        super.onCreate();
        setContext(getApplicationContext());
        databaseHelper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
        databaseHelper.getWritableDatabase();
    }

    public static Context getContext() {
        return context;
    }

    public static void setContext(Context context) {
        App.context = context;
    }

    public static DatabaseHelper getDatabaseHelper() {
        return databaseHelper;
    }

}
