package com.hammadmukhtar.mobilityapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hammadmukhtar.mobilityapp.R;
import com.hammadmukhtar.mobilityapp.adapters.LocationRecyclerViewAdapter;
import com.hammadmukhtar.mobilityapp.data.models.CarLocation;
import com.hammadmukhtar.mobilityapp.interfaces.ILocation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hammad Mukhtar on 02-Dec-17.
 */

public class CarsListFragment extends Fragment implements ILocation {

    private OnListFragmentInteractionListener mListener;
    private final ArrayList<CarLocation> list = new ArrayList<>();
    LocationRecyclerViewAdapter adapter;
    View view;

    public CarsListFragment() {}

    public static CarsListFragment newInstance() {
        return new CarsListFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_list, container, false);

        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            adapter = new LocationRecyclerViewAdapter(list, mListener);
            recyclerView.setAdapter(adapter);
        }
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void updateList(List<CarLocation> places){
        this.list.clear();
        this.list.addAll(places);
        adapter.notifyDataSetChanged();
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(CarLocation item);
    }

}
