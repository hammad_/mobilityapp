package com.hammadmukhtar.mobilityapp.fragments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hammadmukhtar.mobilityapp.R;
import com.hammadmukhtar.mobilityapp.data.models.CarLocation;
import com.hammadmukhtar.mobilityapp.interfaces.ILocation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Hammad Mukhtar on 02-Dec-17.
 */

public class MapFragment extends Fragment implements ILocation, OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private final List<CarLocation> list = new ArrayList<>();
    private final Map<Marker, Integer> markers = new HashMap<>();
    private int activeMarkerIndex = -1;
    private GoogleMap mMap;

    GoogleApiClient mGoogleApiClient;

    public MapFragment() {}

    public static MapFragment newInstance() {
        return new MapFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        MapsInitializer.initialize(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        }
        else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(final Marker marker) {
                if(markers.containsKey(marker)) {
                    int markerId = markers.get(marker);
                    if( markerId == activeMarkerIndex){
                        showAllMarkers();
                        return true;
                    } else {
                        activeMarkerIndex = markerId;
                        hideOtherMarkers();
                    }
                } else {
                    showAllMarkers();
                }
                return false;
            }
        });
        updateMarkers();
        moveToCurrentLocation();
    }

    public void setList(List<CarLocation> list) {
        this.list.clear();
        this.list.addAll(list);
    }

    @Override
    public void updateList(List<CarLocation> places){
        this.list.clear();
        this.list.addAll(places);
        updateMarkers();

    }

    public void updateMarkers() {
        if(mMap != null) {
            mMap.clear();
            for (int i = 0; i < this.list.size(); i++) {
                LatLng latLng = new LatLng(list.get(i).getCoordinates()[1], list.get(i).getCoordinates()[0]);
                Marker newMarker = mMap.addMarker(new MarkerOptions()
                        .position(latLng).title(list.get(i).getName())
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.car32)));
                markers.put(newMarker, i);
            }
        }
    }

    public void hideOtherMarkers(){
        for (Map.Entry<Marker, Integer> entry : markers.entrySet()) {
            if(entry.getValue() != activeMarkerIndex)
                entry.getKey().setVisible(false);
        }
    }

    public void showAllMarkers(){
        activeMarkerIndex = -1;
        for (Map.Entry<Marker, Integer> entry : markers.entrySet()) {
            entry.getKey().setVisible(true);
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {}

    @Override
    public void onConnectionSuspended(int i) {}

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {}

    @Override
    public void onLocationChanged(Location location) {
    }

    public void updateLocation(){
        if (ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
            moveToCurrentLocation();
        }
    }

    public void moveToCurrentLocation() {
        try {
            if (ActivityCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                LocationManager locationManager = (LocationManager)
                        getActivity().getSystemService(Context.LOCATION_SERVICE);

                Location location = null;
                if (locationManager != null) {
                    Criteria criteria = new Criteria();
                    location = locationManager.getLastKnownLocation(
                            locationManager.getBestProvider(criteria, false));
                }

                if (location != null) {
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 12));
                } else {
                    Toast.makeText(getActivity(), "Please turn on Location service.", Toast.LENGTH_SHORT).show();
                }

            }
        }catch (Exception e){
            Log.d("MapFragment", "moveToCurrentLocation: Failed to move to current location");
        }
    }
}
