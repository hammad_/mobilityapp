package com.hammadmukhtar.mobilityapp.activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.hammadmukhtar.mobilityapp.R;
import com.hammadmukhtar.mobilityapp.data.db.delegates.CarLocationDelegate;
import com.hammadmukhtar.mobilityapp.data.models.CarLocation;
import com.hammadmukhtar.mobilityapp.data.models.LocationsRepo;
import com.hammadmukhtar.mobilityapp.data.network.LocationsAPI;
import com.hammadmukhtar.mobilityapp.data.network.NetworkUtil;
import com.hammadmukhtar.mobilityapp.databinding.ActivityMainBinding;
import com.hammadmukhtar.mobilityapp.fragments.CarsListFragment;
import com.hammadmukhtar.mobilityapp.fragments.MapFragment;
import com.hammadmukhtar.mobilityapp.interfaces.ILocation;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Hammad Mukhtar on 02-Dec-17.
 */

public class MainActivity extends BaseActivity implements BottomNavigationView.OnNavigationItemSelectedListener,
        CarsListFragment.OnListFragmentInteractionListener, LoaderManager.LoaderCallbacks<ArrayList<CarLocation>>{

    private ViewPager viewPager;
    ActivityMainBinding V;

    CarsListFragment listFragment = CarsListFragment.newInstance();
    MapFragment mapFragment = MapFragment.newInstance();

    List<Fragment> fragList = new ArrayList<>();
    ArrayList<CarLocation> list = new ArrayList<>();

    public static final String CARS_LIST = "CARS_LIST";
    private static final int CARS_LOADER_ID = 100;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        V.navigation.setSelectedItemId(R.id.navigation_home);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        V = DataBindingUtil.setContentView(this, R.layout.activity_main);

        if(savedInstanceState == null)
            addFragment(listFragment);
        V.navigation.setOnNavigationItemSelectedListener(this);

        if(savedInstanceState != null) {
            list.addAll((ArrayList<CarLocation>) savedInstanceState.getSerializable(CARS_LIST));
        }
        if(list.isEmpty()) {
            list.addAll(getAllLocations());
        }

        if(savedInstanceState == null) {
            if(list.isEmpty())
                showWaitDialog();
            LocationsAPI locationAPI = NetworkUtil.call(LocationsAPI.class);
            locationAPI.getLocations().enqueue(new Callback<LocationsRepo>() {
                @Override
                public void onResponse(Call<LocationsRepo> call, Response<LocationsRepo> response) {
                    hideWaitDialog();
                    List<CarLocation> places = response.body().getPlaceMarks();
                    list.clear();
                    list.addAll(places);
                    new UpdateDatabase(list).execute();
                    updateFragments();
                }

                @Override
                public void onFailure(Call<LocationsRepo> call, Throwable t) {
                    hideWaitDialog();
                    if(!isNetworkAvailable(MainActivity.this)){
                        Toast.makeText(MainActivity.this, "Please connect to internet for latest results.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(MainActivity.this, "Couldn't load latest data.", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } else {
            updateFragments();
        }
        getSupportLoaderManager().initLoader(CARS_LOADER_ID, null, this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getSupportLoaderManager().restartLoader(CARS_LOADER_ID, null, this);
    }

    public void updateFragments(){
        for (int i = 0; i < getSupportFragmentManager().getFragments().size(); i++) {
            if (getSupportFragmentManager().getFragments().get(i) instanceof ILocation)
                ((ILocation) getSupportFragmentManager().getFragments().get(i)).updateList(list);
        }
    }

    @Override
    public void onListFragmentInteraction(CarLocation item) {

    }

    public static final int PERMISSIONS_LOCATION = 1000;
    private void checkLocationPermission() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSIONS_LOCATION);
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSIONS_LOCATION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[], @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSIONS_LOCATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mapFragment.updateLocation();
                } else {
                    Toast.makeText(this, "Permission Denied", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if(!item.isChecked()) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    FragmentManager manager = getSupportFragmentManager();
                    manager.popBackStack();
                    return true;
                case R.id.navigation_dashboard:
                    checkLocationPermission();
                    mapFragment.setList(list);
                    replaceFragment(mapFragment);
                    return true;
            }
        }
        return false;
    }

    public List<CarLocation> getAllLocations(){
        CarLocationDelegate carLocationDelegate = new CarLocationDelegate();
        return carLocationDelegate.getAll();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        outState.putSerializable(CARS_LIST, list);
    }

    @SuppressWarnings("All")
    @Override
    public Loader<ArrayList<CarLocation>> onCreateLoader(int id, Bundle args) {
        return new AsyncTaskLoader<ArrayList<CarLocation>>(this) {

            ArrayList<CarLocation> carsList = null;

            @Override
            protected void onStartLoading() {
                if (carsList != null) {
                    deliverResult(carsList);
                } else {
                    forceLoad();
                }
            }

            @Override
            public ArrayList<CarLocation> loadInBackground() {
                try {
                    CarLocationDelegate carLocationDelegate = new CarLocationDelegate();
                    return carLocationDelegate.getAll();
                } catch (Exception e) {
                    Log.e("LoadingFailed", "Failed to load data");
                    return null;
                }
            }

            public void deliverResult(ArrayList<CarLocation> carsList) {
                this.carsList = carsList;
                super.deliverResult(carsList);
            }
        };
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<CarLocation>> loader, ArrayList<CarLocation> data) {
        list.clear();
        list.addAll(data);
        updateFragments();
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<CarLocation>> loader) {}

    private class UpdateDatabase extends AsyncTask<Void, Void, Void> {

        ArrayList<CarLocation> list;

        public UpdateDatabase(ArrayList<CarLocation> list) {
            this.list = list;
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                CarLocationDelegate carLocationDelegate = new CarLocationDelegate();
                carLocationDelegate.bulkInsert(list);
                List<CarLocation> newList = carLocationDelegate.getAll();
            } catch (Exception e) {
                Log.d("AsyncTaskMain", "doInBackground: " + e.toString());
            }
            return null;
        }
    }
}
