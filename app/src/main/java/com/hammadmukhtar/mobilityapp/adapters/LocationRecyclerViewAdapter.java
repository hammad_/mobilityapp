package com.hammadmukhtar.mobilityapp.adapters;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.hammadmukhtar.mobilityapp.R;
import com.hammadmukhtar.mobilityapp.data.models.CarLocation;
import com.hammadmukhtar.mobilityapp.databinding.ItemLocationBinding;
import com.hammadmukhtar.mobilityapp.fragments.CarsListFragment.OnListFragmentInteractionListener;

import java.util.List;

/**
 * Created by Hammad Mukhtar on 02-Dec-17.
 */

public class LocationRecyclerViewAdapter extends RecyclerView.Adapter<LocationRecyclerViewAdapter.ViewHolder> {

    private final List<CarLocation> list;
    private final OnListFragmentInteractionListener mListener;

    public LocationRecyclerViewAdapter(List<CarLocation> list, OnListFragmentInteractionListener listener) {
        this.list = list;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemLocationBinding V = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout.item_location, parent, false);
        return new ViewHolder(V);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ItemLocationBinding V;

        ViewHolder(ItemLocationBinding V) {
            super(V.getRoot());
            this.V = V;
        }

        void bind(int position){
            CarLocation item = list.get(position);
            V.tvAddress.setText(item.getAddress());

            // Concatenation for clarity
            V.tvName.setText("Name: " + item.getName());
            V.tvEngineType.setText("Engine Type: " + item.getEngineType());
            V.tvExterior.setText("Exterior: " + item.getExterior());
            V.tvInterior.setText("Interior: " + item.getInterior());
            V.tvFuel.setText("Fuel: " + item.getFuel());
            V.tvVIN.setText("VIN: " + item.getVin());
        }
    }
}
