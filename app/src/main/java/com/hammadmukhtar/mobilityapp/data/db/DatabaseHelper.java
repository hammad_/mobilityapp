package com.hammadmukhtar.mobilityapp.data.db;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.hammadmukhtar.mobilityapp.data.models.CarLocation;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

/**
 * Created by Hammad Mukhtar on 02-Dec-17.
 */

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "Mobility.db";
    private static String TAG = "DatabaseHelper";

    private static final int DATABASE_VERSION = 1;

    private Dao<CarLocation, Integer> CarLocationDao = null;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, CarLocation.class);
        } catch (SQLException e) {
            Log.d(this.getClass().getSimpleName(), "Can't create database");
            throw new RuntimeException(e);
        } catch (java.sql.SQLException e) {
            Log.d(this.getClass().getSimpleName(), e.toString());
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {}

    public Dao<CarLocation, Integer> getCarLocationDao() {

        if (null == CarLocationDao) {
            try {
                CarLocationDao = getDao(CarLocation.class);
            }catch (java.sql.SQLException e) {
                Log.d(this.getClass().getSimpleName(), "SQL Exception");
            }
        }
        return CarLocationDao;
    }

}
