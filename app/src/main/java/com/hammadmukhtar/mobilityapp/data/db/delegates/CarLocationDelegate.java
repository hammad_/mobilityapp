package com.hammadmukhtar.mobilityapp.data.db.delegates;

import android.util.Log;

import com.hammadmukhtar.mobilityapp.app.App;
import com.hammadmukhtar.mobilityapp.data.models.CarLocation;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.Dao.CreateOrUpdateStatus;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hammad Mukhtar on 02-Dec-17.
 */

public class CarLocationDelegate {

    private final Dao<CarLocation, Integer> CarLocationDao;
    private final QueryBuilder<CarLocation, Integer> qb;

    private static String TAG = "CarLocationDelegate";

    public CarLocationDelegate() {
        CarLocationDao = App.databaseHelper.getCarLocationDao();
        qb = CarLocationDao.queryBuilder();
    }

    public boolean insertOrUpdate(CarLocation item) {
        try {
            CreateOrUpdateStatus status = CarLocationDao.createOrUpdate(item);
            return (status.isCreated() || status.isUpdated());
        } catch (Exception e) {
            Log.d(TAG, "insertOrUpdate: " + e.toString());
        }
        return false;
    }

    public ArrayList<CarLocation> getAll() {
        try {
            return (ArrayList) qb.query();
        } catch (Exception e) {
            Log.d(TAG, "getAll: " + e.toString());
        }
        return null;
    }

    public void deleteAll() {
        try {
            CarLocationDao.delete(CarLocationDao.queryForAll());
        } catch (SQLException e) {
        }
    }

    public void bulkInsert(List<CarLocation> list){
        App.getDatabaseHelper().getWritableDatabase().beginTransaction();
        try{
            deleteAll();
            for (int i = 0; i < list.size(); i++) {
                insertOrUpdate(list.get(i));
            }
            App.getDatabaseHelper().getWritableDatabase().setTransactionSuccessful();
        }
        finally {
            App.getDatabaseHelper().getWritableDatabase().endTransaction();
        }
    }
}
