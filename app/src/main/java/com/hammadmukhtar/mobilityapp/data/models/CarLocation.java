package com.hammadmukhtar.mobilityapp.data.models;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by Hammad Mukhtar on 02-Dec-17.
 */

@DatabaseTable(tableName = "car_locations")
public class CarLocation implements Serializable {

    @DatabaseField(columnName = "id", generatedId = true)
    private int ID;
    @SerializedName("address")
    @DatabaseField(columnName = "address")
    private String address;
    @SerializedName("coordinates")
    @DatabaseField(columnName = "coordinates", dataType= DataType.SERIALIZABLE)
    private double[] coordinates;
    @SerializedName("engineType")
    @DatabaseField(columnName = "engineType")
    private String engineType;
    @SerializedName("exterior")
    @DatabaseField(columnName = "exterior")
    private String exterior;
    @SerializedName("fuel")
    @DatabaseField(columnName = "fuel")
    private int fuel;
    @SerializedName("interior")
    @DatabaseField(columnName = "interior")
    private String interior;
    @SerializedName("name")
    @DatabaseField(columnName = "name")
    private String name;
    @SerializedName("vin")
    @DatabaseField(columnName = "vin")
    private String vin;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double[] getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(double[] coordinates) {
        this.coordinates = coordinates;
    }

    public String getEngineType() {
        return engineType;
    }

    public void setEngineType(String engineType) {
        this.engineType = engineType;
    }

    public String getExterior() {
        return exterior;
    }

    public void setExterior(String exterior) {
        this.exterior = exterior;
    }

    public int getFuel() {
        return fuel;
    }

    public void setFuel(int fuel) {
        this.fuel = fuel;
    }

    public String getInterior() {
        return interior;
    }

    public void setInterior(String interior) {
        this.interior = interior;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }
}
