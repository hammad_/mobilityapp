package com.hammadmukhtar.mobilityapp.data.network;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.hammadmukhtar.mobilityapp.preferences.WebConstants.BASE_URL;

/**
 * Created by Hammad Mukhtar on 02-Dec-17.
 */

public class NetworkUtil {

    public static <T> T call(Class<T> t){

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        Retrofit.Builder builder =
            new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(
                        GsonConverterFactory.create()
                );

        Retrofit retrofit = builder
            .client(
                httpClient.build()
            ).build();

        return retrofit.create(t);
    }
}
