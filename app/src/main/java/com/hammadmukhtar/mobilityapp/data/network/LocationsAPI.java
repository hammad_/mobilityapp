package com.hammadmukhtar.mobilityapp.data.network;

import com.hammadmukhtar.mobilityapp.data.models.LocationsRepo;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Hammad Mukhtar on 02-Dec-17.
 */

public interface LocationsAPI {

    @GET("locations.json")
    Call<LocationsRepo> getLocations();
}
