package com.hammadmukhtar.mobilityapp.data.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Hammad Mukhtar on 02-Dec-17.
 */

public class LocationsRepo {

    @SerializedName("placemarks")
    private List<CarLocation> placeMarks;

    public List<CarLocation> getPlaceMarks() {
        return placeMarks;
    }

    public void setPlaceMarks(List<CarLocation> placeMarks) {
        this.placeMarks = placeMarks;
    }
}